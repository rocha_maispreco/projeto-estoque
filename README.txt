Configuração do banco: appsettings.json

Telas: 
1 - Login
2 - Cadastro Usuário
3 - Inicio
4 - Cadastro
5 - Relatorio
6 - Perda/Venda
7 - Pesquisar

Login: Email e senha. Validação para Email e senha simplificado. Não há criptografia para senha.

Cadastro Usuário: Validação para Emails já registrados, tipo de email e senha entre 6 e 16 caracteres.

Inicio: Resumo do estoque por Produto/Lote

Cadastro: Cadastrar Produtos. Data de entrada será salva na transação do Produto.

Relatório: Simplificado. Apenas pelo Tipo de Transação com algumas informações.

Perda/Venda: Para acesso a Perda/Venda, se faz necessário pesquisar pelo produto e então clicar em Perda/Venda.

Pesquisar: Pesquisa com Like pelo produto desejado. 2 Opções:
	 Alterar Produto: Possibilidade de alterar para todos os produtos daquele nome a Descrição, categoria e nome - Não possível alterar Data, Lote e Valor. 
	
	 Perda/Venda: Remover produtos do Estoque por perda ou venda.
	

Banco:

TipoPerfil: Administração e Usuário
	Administrador tem acesso a todos o sistema. Nao foi utilizado a classe de autorização do .Net
	Usuário: Acesso a relatório, tela inicial, Perda/Venda. Não poderá adicionar produtos.

TipoTransação: Responsável por registrar os tipos de transações ocorridas no sistema.

View_Relatorios: Agrupar as principais informações para o relatório.