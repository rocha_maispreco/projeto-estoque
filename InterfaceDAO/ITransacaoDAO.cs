﻿using Projeto_Estoque.Elementos;
using Projeto_Estoque.Elementos.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_Estoque.InterfaceDAO
{
    interface ITransacaoDAO
    {
        bool SalvarTransacao(Transacao transacao);


    }
}
