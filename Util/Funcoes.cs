﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_Estoque.Util
{
    public abstract class Funcoes
    {
       public static string geraCodigo(string prefixo)
        {
            string aux = Guid.NewGuid().ToString();
            string CodigoFinal = prefixo + aux.ToString().Replace("-", "").Substring(0,7).ToUpper();
            return CodigoFinal;
        }
    }
}
