﻿using System;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Util;

namespace Projeto_Estoque.Validações
{
    public abstract class ProdutoValidacao
{
        public static ProdutoDTO CarregaCodigo(ProdutoDTO produto)
        {           
            if (String.IsNullOrEmpty(produto?.Codigo))
                produto.Codigo = Funcoes.geraCodigo("PRD-");
            
            return produto;
        }
        public static bool ValidaPreco(ProdutoDTO produto)
        {
            if (produto?.Valor > 0 && produto?.Valor != null)
                return true;

            return false;
        }
        public static bool ValidaNome(ProdutoDTO produto)
        {
            if (!string.IsNullOrEmpty(produto?.Nome))
                return true;
            return false;
        }
        public static bool ValidaCategoria(ProdutoDTO produto)
        {
            if (!string.IsNullOrEmpty(produto?.Categoria))
                return true;
            return false;
        }
        public static bool ValidaLote(ProdutoDTO produto)
        {
            if (!string.IsNullOrEmpty(produto?.Lote))
                return true;
            return false;
        }

        public static bool ValidaQuantidade(ProdutoDTO produto)
        {
            if (produto.Quantidade < 1)
                return false;
            return true;
        }
        
        public static bool ValidaDataEntrada(ProdutoDTO produto)
        {
           
           

            if (produto?.DataEntrada == DateTime.MinValue)
                return false;
            return true;
        }
    }

}