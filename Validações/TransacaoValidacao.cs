﻿using System;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Util;

namespace Projeto_Estoque.Validações
{
    public abstract class TransacaoValidacao
{
        public static bool ValidaQuantidade(TransacaoDTO transacaoDTO)
        {
            if (transacaoDTO.QuantidadeTransacao < 1)
                return false;
            return true;
        }

        public static bool ValidaProdutoIdProduto(TransacaoDTO transacaoDTO)
        {
            if (transacaoDTO.produtoDTO?.IdProduto == null)
                return false;

            return true;
        }
    }

}