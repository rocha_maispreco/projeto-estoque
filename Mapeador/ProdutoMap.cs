﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projeto_Estoque.Elementos;

namespace Projeto_Estoque.Mapeador
{
    public class ProdutoMap : IEntityTypeConfiguration<Produto>
    {

       
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.ToTable("Produto");

            builder.HasKey(x => x.id);
            builder.Property(x => x.id)
            .HasColumnName("ID");
            // .IsRequired();

            builder.Property(x => x.Nome)
            .HasColumnName("NOME")
            .IsRequired();

            builder.Property(x => x.Categoria)
            .HasColumnName("CATEGORIA")
            .IsRequired();

            builder.Property(x => x.Valor)
          .HasColumnName("VALOR")
          .IsRequired();

            builder.Property(x => x.Descricao)
          .HasColumnName("DESCRICAO")
          .IsRequired();


        }
    }
}
