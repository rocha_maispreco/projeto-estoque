﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projeto_Estoque.Elementos;

namespace Projeto_Estoque.Mapeador
{
    public class RelatorioMap : IEntityTypeConfiguration<Relatorio>
    {

       
        public void Configure(EntityTypeBuilder<Relatorio> builder)
        {


            builder.ToTable("VIEW_RELATORIOS");


            
            builder.HasKey(x => x.ID_TRANSACAO);
            builder.Property(x => x.ID_TRANSACAO)
            .HasColumnName("ID");


            builder.Property(x => x.NOME)
            .HasColumnName("NOME")
            .IsRequired();

            builder.Property(x => x.LOTE)
            .HasColumnName("LOTE")
            .IsRequired();

            builder.Property(x => x.CODIGO)
            .HasColumnName("CODIGO")
            .IsRequired();

            builder.Property(x => x.QUANTIDADE_TRANSACAO)
            .HasColumnName("QUANTIDADE_TRANSACAO")
            .IsRequired();

            builder.Property(x => x.QUANTIDADE_ESTOQUE)
           .HasColumnName("QUANTIDADE_ESTOQUE")
           .IsRequired();

            builder.Property(x => x.TIPO_TRANSACAO)
            .HasColumnName("TIPO_TRANSACAO")
            .IsRequired();

            builder.Property(x => x.EMAIL)
            .HasColumnName("EMAIL")
            .IsRequired();

            builder.Property(x => x.DATA_ENTRADA)
            .HasColumnName("DATA_ENTRADA")
            .IsRequired();

            builder.Property(x => x.DATA_SAIDA)
            .HasColumnName("DATA_SAIDA")
            .IsRequired();
        }
    }
}
