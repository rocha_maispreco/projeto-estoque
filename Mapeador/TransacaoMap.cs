﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projeto_Estoque.Elementos;

namespace Projeto_Estoque.Mapeador
{
    public class TransacaoMap : IEntityTypeConfiguration<Transacao>
    {


        public void Configure(EntityTypeBuilder<Transacao> builder)
        {
            builder.ToTable("TRANSACAO_PRODUTO");

            builder.HasKey(x => x.id);
            builder.Property(x => x.id)
            .HasColumnName("ID");
            // .IsRequired();

            builder.Property(x => x.ID_PRODUTO)
            .HasColumnName("ID_PRODUTO")
            .IsRequired();

            builder.Property(x => x.ID_TIPO_TRANSACAO)
            .HasColumnName("ID_TIPO_TRANSACAO")
            .IsRequired();

            builder.Property(x => x.Quantidade)
          .HasColumnName("QUANTIDADE")
          .IsRequired();

            builder.Property(x => x.DataEntrada)
          .HasColumnName("DATA_ENTRADA")
          .IsRequired();

            builder.Property(x => x.DataSaida)
          .HasColumnName("DATA_SAIDA")
          .IsRequired();

            builder.Property(x => x.ID_USUARIO)
            .HasColumnName("ID_USUARIO")
            .IsRequired();

            builder.Property(x => x.DataAtualizacao)
            .HasColumnName("DATA_ATUALIZACAO")
            .IsRequired();
            
        }
    }
}
