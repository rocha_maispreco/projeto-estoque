﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projeto_Estoque.Elementos;

namespace Projeto_Estoque.Mapeador
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {

       
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario");

            builder.HasKey(x => x.id);
            builder.Property(x => x.id)
            .HasColumnName("ID");
            // .IsRequired();

            builder.Property(x => x.email)
            .HasColumnName("EMAIL")
            .IsRequired();

            builder.Property(x => x.Senha)
            .HasColumnName("SENHA")
            .IsRequired();

            builder.Property(x => x.TipoPerfil)
            .HasColumnName("TIPO_PERFIL")
            .IsRequired();

 

        }
    }
}
