﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DAO;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.DAO;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;

namespace Projeto_Estoque.Controllers
{
    public class LoginController : Controller
    {

        private readonly UsuarioDAO _context;
        public LoginController(DataContext dataContext)
        {
            _context = new UsuarioDAO(dataContext);
        }
        public IActionResult Login(Usuario usuario)
        {     
                return View("\\Views\\Home\\Login.cshtml");
        }

        public IActionResult Logar(Usuario usuario)
        {
            
            


            if (!_context.carregarUsuario(usuario))
            {
                ModelState.AddModelError("Email", "Email ou senha estão incorretos");
                ModelState.AddModelError("Senha", "Email ou senha estão incorretos");
            }      
            if (ModelState.IsValid)
            {
                usuario = Usuario.GetInstance();
                return RedirectToAction("CarregarProdutos", "Home");
            }
            else
            {

                return View("\\Views\\Home\\Login.cshtml");
            }
        }

        
        public IActionResult Sair()
        {
            LoginValidacao.SairSistema();
            return Login(Usuario.GetInstance());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
