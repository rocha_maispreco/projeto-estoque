﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DAO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.DAO;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Validações;
using Projeto_Estoque.Enumerador;
namespace Projeto_Estoque.Controllers
{
   
    public class ConsultaController : Controller
    {
       

        private readonly ProdutoDAO _context;
        public ConsultaController(DataContext dataContext)
        {
            _context = new ProdutoDAO(dataContext);
        }

        public IActionResult Consulta(string NomeProduto)
        {

            TipoPerfil tipoPerfil = (TipoPerfil)LoginValidacao.ValidaPerfil();
            ViewData["Message"] = NomeProduto;
            if (tipoPerfil == TipoPerfil.Administrador || tipoPerfil == TipoPerfil.Usuario)
                return View("\\Views\\Home\\Consulta.cshtml");          
            else
            {
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }

        }

        public ActionResult<List<ProdutoDTO>> ConsultaProduto(ProdutoDTO produto)
        {
           
            

            Enumerador.TipoPerfil tipoPerfil = (Enumerador.TipoPerfil) LoginValidacao.ValidaPerfil();

           // List<Produto> listaProdutos =  _context.CarregarProdutos(produto.Nome);
            if (tipoPerfil == Enumerador.TipoPerfil.Usuario || tipoPerfil == Enumerador.TipoPerfil.Administrador)
            {
                
                return View("\\Views\\Home\\Consulta.cshtml");               
            }
            else
            {
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }
                
        }


        public List<ProdutoDTO> ListaPorNome(string nomeProduto)
        {
            //ProdutoDAO produtoDAO = new ProdutoDAO();
           
            List<ProdutoDTO> listaProdutos = _context.CarregarProdutos(nomeProduto);
            return listaProdutos;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
