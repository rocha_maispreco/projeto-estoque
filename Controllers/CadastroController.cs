﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Projeto_Estoque.DAO;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Enumerador;

namespace Projeto_Estoque.Controllers
{
    public class CadastroController : Controller
    {

        private readonly ProdutoDAO _context;
        public CadastroController(DataContext dataContext)
        {
            _context = new ProdutoDAO(dataContext);          
        }
            public IActionResult Cadastro(ProdutoDTO produto)
        {

            TipoPerfil tipoPerfil = (TipoPerfil) LoginValidacao.ValidaPerfil();
            if (tipoPerfil == TipoPerfil.Administrador)
                return View("\\Views\\Home\\Cadastro.cshtml");
            else if (tipoPerfil == TipoPerfil.Usuario)
                return View("\\Views\\Home\\ErroAutorizacao.cshtml");
            else
            {   
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }

        }

        public IActionResult CadastrarProduto(ProdutoDTO produto)
        {
            
            Projeto_Estoque.Enumerador.TipoPerfil tipoPerfil =(Projeto_Estoque.Enumerador.TipoPerfil) LoginValidacao.ValidaPerfil();
            if (tipoPerfil == TipoPerfil.Administrador)
            {
                if (ModelState.TryGetValue("Id", out ModelStateEntry ObjetoValorId))
                    ObjetoValorId.Errors.Clear();

                if (!ProdutoValidacao.ValidaCategoria(produto))
                    ModelState.AddModelError("Categoria", "Categoria do produto é um campo obrigatório!");
                if (!ProdutoValidacao.ValidaNome(produto))
                    ModelState.AddModelError("Nome", "Nome do produto é um campo obrigatório!");
                if (!ProdutoValidacao.ValidaLote(produto))
                    ModelState.AddModelError("Lote", "Lote do produto é um campo obrigatório!");
                if (!ProdutoValidacao.ValidaQuantidade(produto))
                    ModelState.AddModelError("Quantidade", "Quantidade do produto não pode ser menor que 1!");
                if (!ProdutoValidacao.ValidaDataEntrada(produto))
                {
                    if (ModelState.TryGetValue("DataEntrada", out ModelStateEntry ObjetoValor))
                        ObjetoValor.Errors.Clear();
                    
                    ModelState.AddModelError("DataEntrada", "Data de Entrada do produto é um campo obrigatório!");
                }
                if (!ProdutoValidacao.ValidaPreco(produto))
                {
                    if (ModelState.TryGetValue("Valor", out ModelStateEntry ObjetoValor))
                        ObjetoValor.Errors.Clear();
                    ModelState.AddModelError("Valor", "Valor do produto é um campo obrigatório e maior que 0!");
                  
                }
               

                if (ModelState.IsValid)
                {

                    if (_context.SalvarProduto(produto))
                    {
                        ViewData["Message"] = "Produto Cadastrado com sucesso!";
                        return RedirectToAction("Cadastro", new { id = "Sucesso" });
                    }
                    else
                    {
                        ViewData["Message"] = "Erro ao cadastrar o produto!";
                        return View("\\Views\\Home\\Cadastro.cshtml");
                    }
                }
                else
                {
                    ViewData["Message"] = "";
                    return View("\\Views\\Home\\Cadastro.cshtml");
                }
                
            }
            else if (tipoPerfil == TipoPerfil.Usuario)
                return View("\\Views\\Home\\ErroAutorizacao.cshtml");
            else
            {
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }
        }

        
     
     
        

        public IActionResult AlterarProduto(string codigo, string lote)
        {
            ProdutoDTO produto = _context.consultarProduto(codigo, lote);
           
            return View("\\Views\\Home\\Cadastro.cshtml", produto);

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
