﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DAO;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.DAO;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;

namespace Projeto_Estoque.Controllers
{
    public class CadastroUsuarioController : Controller
    {
        private readonly UsuarioDAO _context;
        public CadastroUsuarioController(DataContext dataContext)
        {
            _context = new UsuarioDAO(dataContext);
        }
        public IActionResult Cadastro(Usuario novoUsuario)
        {
            return View("\\Views\\Home\\CadastroUsuario.cshtml");
        }
        public IActionResult Cadastrar(Usuario usuario)
        {
            if (!LoginValidacao.ValidaEmail(usuario.email))
                ModelState.AddModelError("Email", "Email está em um formato incorreto. Exemplo correto: Email@email.com");
            if (!LoginValidacao.ValidaSenha(usuario.Senha))
                ModelState.AddModelError("Senha", "Minímo de 6 caracteres e máximo de 16");
            //LoginValidacao.CarregaTipoPerfil(usuario);
            if (_context.validarEmail(usuario))
            { 
                ModelState.AddModelError("Email", "Email já está cadastrado!");
            }
            if (ModelState.IsValid)
            {
                if (_context.SalvarUsuario(usuario))
                {
                    return RedirectToAction("CarregarProdutos", "Home");
                }
                else
                {
                    ViewData["Title"] = "Erro ao salvar novo Usuário do sistema.";
                    return View("\\Views\\Home\\CadastroUsuario.cshtml");
                }
            }
            else
            {

                return View("\\Views\\Home\\CadastroUsuario.cshtml");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
