﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DAO;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.DAO;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Validações;

namespace Projeto_Estoque.Controllers
{
    public class HomeController : Controller
    {

        private readonly ProdutoDAO _context;
        public HomeController(DataContext dataContext)
        {
            _context = new ProdutoDAO(dataContext);
            CarregarProdutos();
        }
        public IActionResult Index()
        {
           Enumerador.TipoPerfil tipoPerfil = (Enumerador.TipoPerfil) LoginValidacao.ValidaPerfil();

            if (tipoPerfil == Enumerador.TipoPerfil.Usuario || tipoPerfil == Enumerador.TipoPerfil.Administrador)
            {
                return View("\\Views\\Home\\Index.cshtml");
            }
            else
            {
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }
           
        }
        public ActionResult CarregarProdutos()
        {
            List<ProdutoDTO> listaProdutos = _context.listarProdutos();
           
            return View("\\Views\\Home\\Index.cshtml", listaProdutos);

        }






        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
