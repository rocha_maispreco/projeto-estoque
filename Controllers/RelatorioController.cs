﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;
using Projeto_Estoque.Enumerador;
using Projeto_Estoque.DAO;
using ApiService.DAO;

namespace Projeto_Estoque.Controllers
{
    public class RelatorioController : Controller
    {

        private readonly RelatorioDAO _context;
        public RelatorioController(DataContext dataContext)
        {
            _context = new RelatorioDAO(dataContext);
        }
        public IActionResult Relatorio()
        {
            TipoPerfil tipoPerfil = (TipoPerfil) LoginValidacao.ValidaPerfil();
            List<Relatorio> listaRelatorio = _context.carregarRelatorio();
            if(listaRelatorio == null || listaRelatorio.Count < 1)
                return View("\\Views\\Home\\Relatorio.cshtml");
            if (tipoPerfil == TipoPerfil.Usuario || tipoPerfil == TipoPerfil.Administrador)
            {
                
                return View("\\Views\\Home\\Relatorio.cshtml", listaRelatorio);
            }
            else
            {
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }
        }     

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
