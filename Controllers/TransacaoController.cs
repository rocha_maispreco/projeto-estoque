﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;
using Projeto_Estoque.Enumerador;
using Projeto_Estoque.DAO;
using ApiService.DAO;
using Projeto_Estoque.Elementos.DTO;

namespace Projeto_Estoque.Controllers
{
    public class TransacaoController : Controller
    {

        private readonly TransacaoDAO _context;
        private readonly ProdutoDAO _contextProdutoDAO;
        private ProdutoDTO produtoAux;
        public TransacaoController(DataContext dataContext)
        {
            _context = new TransacaoDAO(dataContext);
            _contextProdutoDAO = new ProdutoDAO(dataContext);
        }
        public IActionResult Transacao(string codigo, string lote)
        {

            ProdutoDTO produto = _contextProdutoDAO.consultarProduto(codigo, lote);
            TipoPerfil tipoPerfil = (TipoPerfil) LoginValidacao.ValidaPerfil();
            TransacaoDTO transacaoDTO = new TransacaoDTO();
            transacaoDTO.produtoDTO = produto;
            if (tipoPerfil == TipoPerfil.Usuario || tipoPerfil == TipoPerfil.Administrador)
            {
                return View("\\Views\\Home\\Transacao.cshtml", transacaoDTO);
            }
            else
            {
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }
            
        }
        public IActionResult PerdaVenda(TransacaoDTO transacaoDTO)
        {
            TipoPerfil tipoPerfil = (TipoPerfil)LoginValidacao.ValidaPerfil();

            Transacao transacao = new Transacao();
            transacao.DataSaida = transacaoDTO.DataSaida;
            transacao.Quantidade = transacaoDTO.QuantidadeTransacao;
            transacao.ID_TIPO_TRANSACAO = transacaoDTO.TipoTransacao;
            if(!TransacaoValidacao.ValidaProdutoIdProduto(transacaoDTO))            
            {
                ViewData["Message"] = "Erro ao recuperar o Produto da transação.";
                return View("\\Views\\Home\\Transacao.cshtml");
            }
            else
                transacao.ID_PRODUTO = (int)transacaoDTO.produtoDTO.IdProduto;
            if (transacao.ID_PRODUTO == 0)
            {
                ViewData["Message"] = "Erro ao recuperar o Produto da transação.";
                return View("\\Views\\Home\\Transacao.cshtml");

                //ModelState.AddModelError("QuantidadeTransacao", "Erro ao recuperar o Produto da transação.");
            }

            if(!TransacaoValidacao.ValidaQuantidade(transacaoDTO))
            {
                ModelState.AddModelError("QuantidadeTransacao", "Quantidade não pode ser menor que 1.");
                return View("\\Views\\Home\\Transacao.cshtml");
            }
            transacao.ID_USUARIO = Usuario.GetInstance().id;
            transacao.DataAtualizacao = DateTime.Now;

            produtoAux = _contextProdutoDAO.consultarProduto(transacaoDTO.produtoDTO.Codigo, transacaoDTO.produtoDTO.Lote);

            if ((produtoAux.Quantidade - transacao.Quantidade) < 1)
                ModelState.AddModelError("QuantidadeTransacao", "Quantidade no estoque não é suficiente.");

            if (ModelState.IsValid)
            {
                
                if (!_context.SalvarTransacao(transacao))
                {
                    
                    ModelState.AddModelError("QuantidadeTransacao", "");
                    ViewData["Message"] = "Erro ao Salvar a transação.";
                    return View("\\Views\\Home\\Transacao.cshtml");
                }
                else
                {
                    ViewData["Message"] = "Transação Cadastrada com sucesso!";
                    return RedirectToAction("Transacao", new { id = "Sucesso" });
                }               
            }
            else
            {
                ViewData["Message"] = "";
                return View("\\Views\\Home\\Transacao.cshtml");
            }

        }         
        
        public IActionResult CarregarTransacao(string codigo, string lote)
        {
            ProdutoDTO produto = _contextProdutoDAO.consultarProduto(codigo, lote);
            produtoAux = produto;
            TransacaoDTO transacaoDTO = new TransacaoDTO();
            transacaoDTO.produtoDTO = produto;
            transacaoDTO.Id_Produto =(int) produto.IdProduto;
            return View("\\Views\\Home\\Cadastro.cshtml", produto);

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
