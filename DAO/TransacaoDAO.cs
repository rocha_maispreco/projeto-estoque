﻿using ApiService.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.InterfaceDAO;
using Projeto_Estoque.Mapeador;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Util;

namespace Projeto_Estoque.DAO
{
    public class TransacaoDAO : ITransacaoDAO
    {
        private readonly DataContext _context;

        public TransacaoDAO(DataContext context)
        {
            _context = context;
        }
        public bool SalvarTransacao(Transacao transacao)
        {
            try
            {
                Produto produtoAux = _context.ProdutoContext.Where(t => t.id == transacao.ID_PRODUTO).FirstOrDefault();
                if (produtoAux != null)
                {
                    transacao.DataAtualizacao = DateTime.Now;
                    _context.Add(transacao);
                    _context.SaveChanges();
                    if (transacao.ID_TIPO_TRANSACAO != (int)Enumerador.TipoTransacao.Adicao)
                    {
                        Produto produto = new Produto();
                        produto = produtoAux;
                        _context.Entry(produtoAux).State = EntityState.Detached;
                        produto.Quantidade = produto.Quantidade - transacao.Quantidade;
                        _context.Entry(produto).State = EntityState.Modified;
                        _context.SaveChanges();
                    }
                    return true;
                }
                else
                    return false;
                    
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return false;
                //throw;
            }
        }
        


    }
}
