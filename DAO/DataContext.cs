﻿using Microsoft.EntityFrameworkCore;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Mapeador;

namespace ApiService.DAO
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.ApplyConfiguration(new ProdutoMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new TransacaoMap());
            modelBuilder.ApplyConfiguration(new RelatorioMap());

        }

        public DbSet<Produto> ProdutoContext { get; set; }
        public DbSet<Usuario> UsuarioContext { get; set; }
        public DbSet<Transacao> TransacaoContext { get; set; }
        public DbSet<Relatorio> RelatorioContext { get; set; }
    }
}
