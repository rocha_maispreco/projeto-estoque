﻿using ApiService.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.InterfaceDAO;
using Projeto_Estoque.Mapeador;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Util;

namespace Projeto_Estoque.DAO
{
    public class UsuarioDAO : IUsuarioDAO
    {
       private readonly DataContext _context;

        public UsuarioDAO(DataContext context)
        {
            _context = context;
        }
        public bool carregarUsuario(Usuario usuario)
        {
            usuario = _context.UsuarioContext.Where(T => T.Senha == usuario.Senha && T.email == usuario.email).FirstOrDefault();
            if (usuario != null)
            {
                Usuario.SetInstance(usuario);
                return true;
            }
            
            else
                return false;
        }
        public bool validarEmail(Usuario usuario)
        {
            usuario = _context.UsuarioContext.Where(T =>T.email == usuario.email).FirstOrDefault();
            if (usuario != null)
            {
                
                //Usuario.SetInstance(usuario);
                return true;
            }

            else
                return false;
        }

        public bool SalvarUsuario(Usuario usuario)
        {
            try
            {
                _context.UsuarioContext.Add(usuario);
                _context.SaveChanges();
                Usuario.SetInstance(usuario);
                return true;
            }
            catch(Exception e)
            {
                e.ToString();
                return false;
            }
        }      
    }
}
