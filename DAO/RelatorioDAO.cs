﻿using ApiService.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.InterfaceDAO;
using Projeto_Estoque.Mapeador;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Util;

namespace Projeto_Estoque.DAO
{
    public class RelatorioDAO : IRelatorioDAO
    {
       private readonly DataContext _context;

        public RelatorioDAO(DataContext context)
        {
            _context = context;
        }
        public List<Relatorio> carregarRelatorio()
        {
            var relatorios = _context.RelatorioContext.ToList();
            return relatorios;
        }
    }
}
