﻿using ApiService.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.InterfaceDAO;
using Projeto_Estoque.Mapeador;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projeto_Estoque.Elementos.DTO;
using Projeto_Estoque.Util;

namespace Projeto_Estoque.DAO
{
    public class ProdutoDAO : IProdutoDAO 
    {
       private readonly DataContext _context;

        public ProdutoDAO(DataContext context)
        {
            _context = context;
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("ProjetoEstoqueDbConnection");
        //}
        public ProdutoDAO() 
        {
           
        }
      
        public bool SalvarProduto(ProdutoDTO produtoDTO)
        {
            bool salvarTransacao = false;
            int quantidade = produtoDTO.Quantidade;
            try
            {
               Produto produtoAux = _context.ProdutoContext.Where(T => T.id == produtoDTO.IdProduto).DefaultIfEmpty().FirstOrDefault();
             
                Produto produto = parseDTO(produtoDTO);
                //Atualizando Produto
                if (produtoAux != null)
                {
                    List<Produto> listaProdutos = _context.ProdutoContext.Where(T => T.Codigo == produtoAux.Codigo).DefaultIfEmpty().ToList();
                    foreach (Produto item in listaProdutos)
                    {
                        Produto itemParaAtualizar = item;
                        item.Categoria = produto.Categoria;
                        //item.Descricao = produto.Descricao;
                        item.Nome = produto.Nome;
                        item.Valor = produto.Valor;
                        //if (item.Lote == produto.Lote)
                        //{
                        //    salvarTransacao = true;
                        //    item.Quantidade += produto.Quantidade;
                        //}
                        _context.Entry(itemParaAtualizar).State = EntityState.Detached;

                        _context.Entry(item).State = EntityState.Modified;
                        //_context.Update(produto);
                        _context.SaveChanges();
                    }
                    // detach
                    
                    
                }
                //Salvando Produto
                else
                {
                    salvarTransacao = true;
                    produtoAux = _context.ProdutoContext.Where(T => T.Nome == produtoDTO.Nome).DefaultIfEmpty().FirstOrDefault();
                    if (produtoAux != null)
                    {
                        produto.Codigo = produtoAux.Codigo;
                        produto.Categoria = produtoAux.Categoria;
                    }
                    else
                        produto.Codigo = Funcoes.geraCodigo("PRD-");
                    //Mesmo lote, atualizar o produto.
                    if (produtoAux?.Lote == produto.Lote)
                    {
                        produto.id = produtoAux.id;
                        produto.Quantidade += produtoAux.Quantidade;
                        _context.Entry(produtoAux).State = EntityState.Detached;

                        _context.Entry(produto).State = EntityState.Modified;
                        //_context.Update(produto);
                        _context.SaveChanges();

                    }
                    else
                    {
                       
                        produto.id = 0;

                        _context.Add(produto);
                        _context.SaveChanges();
                        
                    }
                }
                if (salvarTransacao)
                {
                    Usuario usuario = Usuario.GetInstance();
                    Transacao transacao = new Transacao();
                    transacao.ID_PRODUTO = produto.id;
                    transacao.ID_TIPO_TRANSACAO = (int)Enumerador.TipoTransacao.Adicao;
                    transacao.Quantidade = produtoDTO.Quantidade;
                    transacao.DataEntrada = produtoDTO.DataEntrada;
                    transacao.ID_USUARIO = usuario.id;
                    TransacaoDAO transacaoDAO = new TransacaoDAO(_context);
                    if (!transacaoDAO.SalvarTransacao(transacao))
                        return false;
                }

                return true;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return false;
                //throw;
            }
           
         
        }
        public Produto parseDTO(ProdutoDTO produtoDTO)
        {
            Produto produto = new Produto();
            produto.Categoria = produtoDTO.Categoria;
            produto.Codigo = produtoDTO.Codigo;
            produto.Descricao = produtoDTO.Descricao;           
            produto.Lote = produtoDTO.Lote;
            produto.Nome = produtoDTO.Nome;
            produto.Valor = produtoDTO.Valor;
            produto.Quantidade = produtoDTO.Quantidade;
            return produto;
        }
        public ProdutoDTO convertDTO(Produto produto)
        {
            ProdutoDTO produtoDTO = new ProdutoDTO();
            produtoDTO.Categoria = produto.Categoria;
            produtoDTO.Codigo = produto.Codigo;
            produtoDTO.Descricao = produto.Descricao;
            produtoDTO.IdProduto = produto.id;
            produtoDTO.Lote = produto.Lote;
            produtoDTO.Nome = produto.Nome;
            produtoDTO.Valor = produto.Valor;
            produtoDTO.Quantidade = produto.Quantidade;
            return produtoDTO;
        }
        public  List<ProdutoDTO> CarregarProdutos(string NomeProduto)
        {
            try
            {
                List<Produto> listaProdutos = _context.ProdutoContext.Where(t => t.Nome.Contains(NomeProduto)).DefaultIfEmpty().ToList();
                List<ProdutoDTO> listaProdutosDTO = new List<ProdutoDTO>();
                foreach (Produto produto in listaProdutos)
                {
                    listaProdutosDTO.Add(convertDTO(produto));
                }
                return listaProdutosDTO;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return null;
                //throw;
            }
           
        }

        public ProdutoDTO consultarProduto(string codigo, string lote)
        {
            try
            {
                ProdutoDTO produtoDTO = convertDTO(_context.ProdutoContext.Where(t => t.Codigo == codigo && t.Lote == lote).DefaultIfEmpty().First());
                return produtoDTO;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return null;
                //throw;
            }

        }
        public List<ProdutoDTO> listarProdutos()
        {
            try
            {
                List<Produto> produtos = _context.ProdutoContext.DefaultIfEmpty().ToList();
                List<ProdutoDTO> listaProdutoDTO = new List<ProdutoDTO>();
                foreach (Produto item in produtos)
                {
                    listaProdutoDTO.Add(convertDTO(item));
                }               
                return listaProdutoDTO;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return null;
                //throw;
            }

        }
        public bool AlterarProduto(Produto produto)
        {
            try
            {  
                _context.Update(produto);
                return true;
            }
            catch (Exception)
            {
                return false;
                //throw;
            }
        }

       
    }
}
