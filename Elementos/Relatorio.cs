using System;
using Projeto_Estoque.Enumerador;
namespace Projeto_Estoque.Elementos
{
    public class Relatorio
    {
        public int ID_TRANSACAO { get; set; }
        public string NOME { get; set; }
        public string LOTE { get; set; }
        public string CODIGO { get; set; }
        public int QUANTIDADE_ESTOQUE { get; set; }
        public int QUANTIDADE_TRANSACAO { get; set; }
        public string TIPO_TRANSACAO { get; set; }
        public string EMAIL { get; set; }
        public DateTime? DATA_ENTRADA { get; set; }
        public DateTime? DATA_SAIDA { get; set; }

        public DateTime DATA_ATUALIZACAO { get; set; }

    }
}