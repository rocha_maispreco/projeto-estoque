﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_Estoque.Elementos.DTO
{
    public class ProdutoDTO
    {
        public int? IdProduto { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Codigo { get; set; }
        public double Valor { get; set; }
        public string Categoria { get; set; }
        public string Lote { get; set; }
        public int Quantidade { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime DataEntrada { get; set; }
    }
}
