﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_Estoque.Elementos.DTO
{
    public class TransacaoDTO
    {
        public ProdutoDTO produtoDTO { get; set; }
       
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime DataSaida { get; set; }
        public int QuantidadeTransacao { get; set; }
        public int TipoTransacao { get; set; }

        public int Id_Produto { get; set; }
    }
}
