using System;

namespace Projeto_Estoque.Elementos
{
    public class Transacao
    {

        public int id { get; set; }
        public int ID_PRODUTO { get; set; }
        public int ID_TIPO_TRANSACAO { get; set; }
        public int Quantidade { get; set; }
        public DateTime? DataEntrada { get; set; }
        public DateTime? DataSaida { get; set; }
        public int ID_USUARIO { get; set; }
        public DateTime DataAtualizacao { get; set; }




    }
}