using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_Estoque.Elementos
{
    public class Produto
    {
        public int id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Codigo { get; set; }
        public double Valor { get; set; }
        public string Categoria { get; set; }
        public string Lote { get; set; }
        public int Quantidade { get; set; }
    }
}