using System;
using Projeto_Estoque.Enumerador;
namespace Projeto_Estoque.Elementos
{
    public class Usuario
    {
        private static Usuario _instance;
        public static Usuario GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Usuario();
                _instance.TipoPerfil = (int) Projeto_Estoque.Enumerador.TipoPerfil.NaoConectado;
            }
            return _instance;
        }
        public static void SetInstance(Usuario usuario)
        {
            _instance = usuario;
        }
        public static void DisconectarUsuario()
        {
            _instance.TipoPerfil = (int)Projeto_Estoque.Enumerador.TipoPerfil.NaoConectado;
        }
        public int id { get; set; }  
        public string Senha { get; set; }
        public string email { get; set; }
        public int TipoPerfil { get; set; }




}
}